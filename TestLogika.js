// Fungsi Lower Case
function lowerCase(str) {
  let result = '';
  for (let i = 0; i < str.length; i++) {
    let charCode = str.charCodeAt(i);
    if (charCode >= 65 && charCode <= 90) {
      charCode += 32;
    }
    result += String.fromCharCode(charCode);
  }
  return result;
}

//Fungsi Split
function stringSplit(spl,a) {
    let result = [];
    let word = '';
  
    for (let i = 0; i < spl.length; i++) {
      if (spl[i] === a) {
        result.push(word);
        word = '';
      } else {
        word += spl[i];
      }
    }
    result.push(word);
  
    return result;
  }

//Fungsi Join
  function stringJoin(a) {
    var result = "";
  
    for (var i = 0; i < a.length; i++) {
      result += a[i];
  
      if (i !== a.length - 1) {
        result += " ";
      }
    }
  
    return result;
  }

//Fungsi Replace
  function stringReplace(rpl,a,b) {
    let result = '';
    
    for (let i = 0; i < rpl.length; i++) {
      if (rpl[i] === a) {
        result += b;
      } else {
        result += rpl[i];
      }
    }
    
    return result;
  }



function convertString(str) {
  str = lowerCase(str);
  const words = stringSplit(str," ");
  for (let i = 0; i < words.length; i++) {
    let word = words[i].replace(/[^a-zA-Z0-9]/g, '');
    word = word.charAt(0).toUpperCase() + word.slice(1);
    words[i] = word;
  }
  const titleCase = stringJoin(words);
  const kebabCase = stringReplace(titleCase," ","-");

  return {
    titleCase,
    kebabCase
  };
}

  
const input = 'SELamAt PaGi Dunia!!';
const result = convertString(input);
console.log(result.titleCase); 
console.log(result.kebabCase); 
console.log("==============================================")


const paragraph = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
const characterCount = {};

for (let i = 0; i < paragraph.length; i++) {
  const character = lowerCase(paragraph[i]); 
  if (/^[a-z ]$/.test(character)) {
    characterCount[character] = (characterCount[character] || 0) + 1;
  }
}
for (const character in characterCount) {
  console.log(`${character}: ${characterCount[character]}`);
}
console.log("==============================================")


function deretPertama(n) {
    let result = "";
    let num = 0;
    for (let i = 0; i < n; i++) {
      result += num + " ";
      num += i * 2 + 1;
    }
    console.log(result);
  }

  function deretKedua(n) {
    let result = "";
    let num = 1;
    for (let i = 0; i < n; i++) {
      result += num + " ";
      num += 1+ i*2;
    }
    console.log(result);
  }
  
  function deretKetiga(n) {
    let result = "";
    let num1 = 0;
    let num2 = 1;
    for (let i = 0; i < n; i++) {
      result += num1 + " ";
      const nextNum = num1 + num2;
      num1 = num2;
      num2 = nextNum;
    }
    console.log(result);
  }
  
  function deretKeempat(n) {
    let result = "";
    let num1 = 0;
    let num2 = 0;
    let num3 = 1;
    for (let i = 0; i < n; i++) {
      result += num1 + " ";
      const nextNum = num1 + num2 + num3;
      num1 = num2;
      num2 = num3;
      num3 = nextNum;
    }
    console.log(result);
  }


  const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
  });
  

  function hitungTotal(angka) {
    let total = 0;
    for (let i = 0; i < angka.length; i++) {
      total += angka[i];
    }
    return total;
  }
  
  function cariTerbesar(angka) {
    let terbesar = angka[0];
    for (let i = 1; i < angka.length; i++) {
      if (angka[i] > terbesar) {
        terbesar = angka[i];
      }
    }
    return terbesar;
  }
  
  function cariTerkecil(angka) {
    let terkecil = angka[0];
    for (let i = 1; i < angka.length; i++) {
      if (angka[i] < terkecil) {
        terkecil = angka[i];
      }
    }
    return terkecil;
  }
  
  let input_string = '20,21,80a,20,5d5,31,23';
  let angka = [];
  let elemen = stringSplit(input_string,",");


  for (let i = 0; i < elemen.length; i++) {      
      let angka_elemen = parseInt(elemen[i]);
        if (!isNaN(angka_elemen)) { 
          angka.push(angka_elemen); 
        }
    
  }
  
  // Menghitung nilai total dari deret angka
  let total = hitungTotal(angka);
  let terbesar = cariTerbesar(angka);
  let terkecil = cariTerkecil(angka);
  let rata_rata = total / angka.length;
  
  

  console.log("Nilai total dari deret angka adalah:", total);
  console.log("Nilai terbesar dari deret angka adalah:", terbesar);
  console.log("Nilai terkecil dari deret angka adalah:", terkecil);
  console.log("Nilai rata-rata dari deret angka adalah:", rata_rata);
  console.log("==============================================")
  
  readline.question('Masukkan Jumlah baris:', n => {
    deretPertama(parseInt(n));
    deretKedua(parseInt(n));
    deretKetiga(parseInt(n));
    deretKeempat(parseInt(n));
    readline.close();
  });